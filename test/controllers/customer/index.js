const getCustomer = require('./getCustomer');
const createCustomer = require('./createCustomer');
module.exports = {
    getCustomer,
    createCustomer
}