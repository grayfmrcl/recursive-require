const assert = require('assert');
const path = require('path');

const loadFiles = require('../index');

describe ('loadFiles', () => {
    it ('should return empty object when the directory is not valid', () => {
        const result = loadFiles('./not-exist-directory');
        assert.deepEqual(result, {});
    });
    it ('should return object of exported values in each file', () => {
        const result = loadFiles(path.join(__dirname, '/controllers/auth'));
        const expected = {
            login: 'login function',
            refresh: 'refresh function'
        }
        assert.deepEqual(result, expected);
    });
    it ('should return object of exported values in index file only if there is index file', () => {
        const result = loadFiles(path.join(__dirname, '/controllers/customer'));
        const expected = {
            getCustomer: 'getCustomer function',
            createCustomer: 'createCustomer function'
        }
        assert.deepEqual(result, expected);
    });
    it('should return files in the nested directory', () => {
        const result = loadFiles(path.join(__dirname, '/controllers'));
        const expected = {
            ip: '127.0.0.1',
            path: '/',
            login: 'login function',
            refresh: 'refresh function',
            getCustomer: 'getCustomer function',
            createCustomer: 'createCustomer function',
            getOrder: 'getOrder function',
            createOrder: 'createOrder function',
            getOrderDetail: 'getOrderDetail function',
            createOrderDetail: 'createOrderDetail function'
        }
        assert.deepEqual(result, expected);
    });
});