const fs = require('fs');
const path = require('path');

module.exports = (dirname, index = 'index') => {
    const result = { };

    function load(dirname) {   
        fs.readdirSync(dirname)
            .forEach(item => {
                const itemPath = path.join(dirname, item);
                const stat = fs.statSync(itemPath);
                if (stat.isFile()) {
                    const exported = require(itemPath);
                    if (typeof exported === 'object') {
                        Object.assign(result, exported);
                    } else {
                        const fileName = path.basename(item, '.js');
                        result[fileName] = require(itemPath);
                    }
                } else if (stat.isDirectory()) {
                    return load(itemPath);
                }
            });
    }

    if (fs.existsSync(dirname)) {
        load(dirname);
    }

    return result;
}